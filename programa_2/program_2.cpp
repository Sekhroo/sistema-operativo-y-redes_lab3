#include <chrono>
#include <pthread.h>
#include <unistd.h>
#include <iostream>

using namespace std;

#include "Class_funtion.h"

// Función que llama a la clase y envia el nombre del archivo
void *analisis (void *param) {
    char *filename = (char *) param;

    Class_funtion analisis_txt(filename);

    sleep (1);

    pthread_exit(0);
}

// Main
int main (int argc, char **argv) {

    // Número de hebras para crear
    pthread_t threads[argc - 1];
    
    // Seudo tabla
    cout << "| Lineas | Palabras | Caracteres | Archivo |" << endl;

    /* Crea todos los hilos */
    for (int i=0; i < argc - 1; i++) {
        pthread_create(&threads[i], NULL, analisis, argv[i+1]);
    }

    /* Para esperar por el término de todos los hilos */
    for (int i=0; i< argc - 1; i++) {
        pthread_join(threads[i], NULL);
    }

    return 0;
}



/* 
    "/usr/bin/time -f %e (./programa)"
o   
    
    #include <chrono>
    auto end_t = chrono:system

    cat "archivo" | wc -l
    ' ' ' ' -c

*/