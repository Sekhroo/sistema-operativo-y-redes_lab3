#ifndef CLASS_FUNTION_H // previnir inclusiones de multiples archivos header
#define CLASS_FUNTION_H

using namespace std;
#include <iostream>

// Declarar clase Fork
class Class_funtion {

    private:
        char *filename; // Variable para guardar la URL del video

        pid_t pid; // Variable para crear el hijo

    public:
        // Constructor base
        Class_funtion(char *filename);

        // Creación de un proceso hijo
        void crear_subproceso();
        
        // Creación de un proceso hijo
        void contar();
};

#endif