using namespace std;

#include <pthread.h>
#include <unistd.h>
#include <sys/wait.h>
#include <iostream>

#include "Class_funtion.h"

Class_funtion::Class_funtion (char *filename) {
  this->filename = filename;
  crear_subproceso();
  contar();
  pthread_exit(0);
}

void Class_funtion::crear_subproceso() {
  // Crea el proceso hijo.
  pid = fork();
}

void Class_funtion::contar() {

  if (pid < 0) { // Valida la creación de proceso hijo.
    cout << "No se pudo crear el sub-proceso ..." << endl;
        
  } else if (pid == 0) { // Código del proceso hijo.
    char* param[] = {"wc",this->filename,"-l","-w","-c", NULL};
    execvp ("wc", param);

    sleep(1);
        
  } else {

    wait (NULL);
  }
}

// Desarrollo:
/* Comandos para contar las lineas, palabras y caracteres

  ->LINEAS:
  char* param[] = {"wc",this->filename,"-l", NULL};
  execvp ("wc", param);

  ->CARACTERES:
  char* param[] = {"wc",this->filename,"-c", NULL};
  execvp ("wc", param);

  ->PALABRAS:
  char* param[] = {"wc",this->filename,"-c", NULL};
  execvp ("wc", param);
*/