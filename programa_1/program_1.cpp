//#include <chrono>

#include <iostream>
using namespace std;

#include "Class_funtion.h"

// Main
int main (int argc, char **argv) {

    //auto start = chrono::system_clock::now();
    
    cout << "| Lineas | Palabras | Caracteres | Archivo |" << endl;

    for (int i=1; i<argc; i++) {
        Class_funtion analisis_txt(argv[i]);
    }

    //auto end = chrono::system_clock::now();

    //auto elapsed_seconds = end-start;

    //cout << endl << endl <<"El programa demoro: " << elapsed_seconds.count() << endl;

    return 0;
}

// Anotaciones:
/* 
    "/usr/bin/time -f %e (./programa)"
o   
    
    #include <chrono>
    auto end_t = chrono:system

    cat "archivo" | wc -l
    ' ' ' ' -c

*/